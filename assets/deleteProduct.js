let params = new URLSearchParams(window.location.search);
let productId = params.get("productId");

let token = localStorage.getItem("token");

fetch(
  `https://paintura-service.onrender.com/api/products/${productId}/delete`,
  {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
)
  .then((result) => result.json())
  .then((result) => {
    if (result) {
      alert(`Product Succesfully Deleted!`);
      window.location.replace(`./products.html`);
    } else {
      alert(`Something went wrong.`);
    }
  });
