let params = new URLSearchParams(window.location.search);
let productId = params.get("productId");

let token = localStorage.getItem("token");

fetch(
  `https://paintura-service.onrender.com/api/products/${productId}/unarchive`,
  {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
)
  .then((result) => result.json())
  .then((result) => {
    if (result) {
      window.location.replace(`./products.html`);
      Swal.fire(`product Succesfully Unarchived!`);
    } else {
      Swal.fire(`Something went wrong.`);
    }
  });
