let params = new URLSearchParams(window.location.search);
let productId = params.get("productId");

let token = localStorage.getItem("token");

fetch(
  `https://paintura-service.onrender.com/api/products/${productId}/archive`,
  {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
)
  .then((result) => result.json())
  .then((result) => {
    if (result === true) {
      alert(`Product Succesfully Archived!`);
      window.location.replace(`./products.html`);
    } else {
      alert(`Something went wrong.`);
    }
  });
