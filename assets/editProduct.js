let editProduct = document.querySelector("#editProduct");

let params = new URLSearchParams(window.location.search);
let productId = params.get("productId");

let token = localStorage.getItem("token");

let name = document.querySelector("#name");
let price = document.querySelector("#price");
let desc = document.querySelector("#desc");

fetch(`https://paintura-service.onrender.com/api/products/${productId}`, {
  method: "GET",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    name.value = result.name;
    price.value = result.price;
    desc.value = result.desc;
  });

editProduct.addEventListener("submit", (e) => {
  e.preventDefault();

  name = name.value;
  desc = desc.value;
  price = price.value;

  fetch(
    `https://paintura-service.onrender.com/api/products/${productId}/edit`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: name,
        price: price,
        desc: desc,
      }),
    }
  )
    .then((result) => result.json())
    .then((result) => {
      if (result !== "undefined") {
        alert(`Product Succesfully updated!`);
        window.location.replace(`./products.html`);
      } else {
        alert(`Product not updated!`);
      }
    });
});
