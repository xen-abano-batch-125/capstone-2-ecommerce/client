//DOM Manipulation

let profile = document.querySelector('#profile');
let navSession = document.querySelector('#navSession');
let registerLink = document.querySelector('#register');
let userToken = localStorage.getItem('token');
//let undefinedUserToken = localStorage.getItem('token') === "undefined";
let admin = localStorage.getItem('isAdmin');

if  (!userToken){
	navSession.innerHTML = 
	`
		<li class="nav-item">
			<a href="./login.html" class="nav-link">SIGN IN</a>
		</li>
	`

	registerLink.innerHTML =
	`
		<li class="nav-item">
			<a href="./register.html" class="nav-link">REGISTER</a>
		</li>
	`
} else {

	if(admin === "false" ){
		profile.innerHTML =
		`
			<li class="nav-item">
				<a href="./myOrders.html" class="nav-link">My Orders</a>
			</li>
		`
		navSession.innerHTML =
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link">Logout</a>
			</li>
		`
	} else {

		navSession.innerHTML =
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link">Logout</a>
			</li>
		`
	}

}
