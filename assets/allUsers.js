let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let seeAllUsersButton = document.getElementById("seeAllUsersButton");
let seeAllUsersContainer = document.querySelector("#seeAllUsersContainer");
let cardFooter;

fetch("https://paintura-service.onrender.com/api/users/allUser", {
  method: "GET",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    if (adminUser === true) {
      result.map((user) => {
        if (user.isAdmin === false) {
          cardFooter = `
					<a href="./allUsers.html?userId=${user._id}" class="btn btn-dark btn-block rounded-0 border-top-0 border-left-0 border-right-0 setAdminButton">
						Set as admin
					</a>
				`;
        } else {
          cardFooter = `
					<h5 class="card-title text-center font-weight-bold">ADMIN</h5>
				`;
        }

        let card = `
					<div class="col-12 col-md-4 my-3">
						<div class="card" style="width: 18rem;">
						  <div class="card-body">
						    <h5 class="card-title text-center font-weight-bold">${user.name}</h5>
						    <p class="card-text text-center">${user.email}</p>
							<p class="card-text text-center">Admin: ${user.isAdmin}</p>
						  </div>
						  <div class="card-footer">
						  	${cardFooter}
						  </div>
						</div>
					</div>

					`;
        seeAllUsersContainer.insertAdjacentHTML("beforeend", card);
      });
    }
  });
