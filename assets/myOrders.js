let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin")==="true";
let myOrdersButton = document.getElementById('myOrdersButton');
let myOrdersContainer = document.querySelector('#myOrdersContainer');
let finalAmountHTML = document.querySelector('#totalAmount');
let finalAmount = 0;

fetch('https://paintura-service.onrender.com/api/users/myOrders',
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {

	if (adminUser === false) {
		result.orders.map( (order) => {

			let productId = order.productId
			let purchasedOn = order.purchasedOn

			fetch(`https://paintura-service.onrender.com/api/products/${productId}`, 
				{
					method: "GET",
					headers: {
						"Authorization": `Bearer ${token}`
					}
				}
			)
			.then( result => result.json())
			.then( result => {

				finalAmount += result.price

				let card = 

					`
					<table class="table text-left">
					  <tbody>
					    <tr>
					      <th scope="row">${purchasedOn}</th>
					      <td>${result.name}</td>
					      <td>${result.desc}</td>
					      <td>PHP ${result.price}</td>
					    </tr>
					  </tbody>
					</table>

					`
						myOrdersContainer.insertAdjacentHTML("beforeend", card);

						totalAmount.innerHTML = 
						`
						<div class="col-12">
						<p class="card-text text-right font-weight-bolder">TOTAL AMOUNT: PHP ${finalAmount}</p>
						</div>
							

						`
			})

		})
	}
})


/*						<div class="col-md-6 mb-5">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title font-weight-bold text-center">
										${result.name}
									</h5>
									<p class="card-text text-center">
										PHP ${result.price}
									</p>
									<p class="card-text text-center">
										${result.desc}
									</p>
									<p class="card-text text-center">Purchased Date: 
										${purchasedOn}
									</p>
								</div>
							</div>
						</div>*/