let params = new URLSearchParams(window.location.search);

let productId = params.get("productId");

let name = document.querySelector("#name");
let desc = document.querySelector("#desc");
let price = document.querySelector("#price");
let customerContainer = document.querySelector("#customerContainer");

let token = localStorage.getItem("token");

fetch(`https://paintura-service.onrender.com/api/products/${productId}`, {
  method: "GET",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    name.innerHTML = result.name;
    price.innerHTML = result.price;
    desc.innerHTML = result.desc;

    customerContainer.innerHTML = `
		<button id="orderButton"class="btn btn-dark btn-block rounded-0 border-top-0 border-left-0 border-right-0">Order</button>
	`;

    //make order button work
    let orderButton = document.querySelector("#orderButton");

    orderButton.addEventListener("click", () => {
      fetch("https://paintura-service.onrender.com/api/users/checkout", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          productId: productId,
        }),
      })
        .then((result) => result.json())
        .then((result) => {
          if (result) {
            alert("Ordered successfully!");
            window.location.replace(`./products.html`);
          } else {
            alert("Something went wrong.");
          }
        });
    });
  });
