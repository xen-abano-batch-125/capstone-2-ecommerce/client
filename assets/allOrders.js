let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let allOrdersButton = document.getElementById("allOrdersButton");
let orderContainer = document.querySelector("#orderContainer");

fetch("https://paintura-service.onrender.com/api/users/orders", {
  method: "GET",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    if (adminUser === true) {
      result.map((order) => {
        let card = `

					<table class="table">
					<tbody>
					    <tr>
					      <td>Order ID: ${order._id}</th>
					      <td>Date Purchased: ${order.purchasedOn}</td>
					      <td>User ID: ${order.userId}</td>
					      <td>Product ID: ${order.productId}</td>
					    </tr>
					<tbody>
					</table>

					`;
        orderContainer.insertAdjacentHTML("beforeend", card);
      });
    }
  });
