let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let adminButton = document.getElementById('adminButton');
let allOrdersButton = document.getElementById('allOrdersButton');
let myOrdersButton = document.getElementById('myOrdersButton');
let seeAllUsersButton = document.getElementById('seeAllUsersButton');

let cardFooter;

if(adminUser === false || adminUser === null){
	adminButton.innerHTML =
	`

	`
	myOrdersButton.innerHTML = 
	`
		<div class="col-md-6 offset-md-3 mb-4">
			<a href="./myOrders.html" class="btn btn-dark rounded-0">
				MY ORDERS
			</a>
		</div>
	`
} else {

	adminButton.innerHTML =
	`
		<div class="col-md-6 offset-md-3 my-2">
			<a href="./addProduct.html" class="btn btn-dark btn-block rounded-0">
				CREATE PRODUCT
			</a>
		</div>
	`
	allOrdersButton.innerHTML =
	`
		<div class="col-md-6 offset-md-3 my-2">
			<a href="./allOrder.html" class="btn btn-dark btn-block rounded-0">
				ALL ORDERS
			</a>
		</div>
	`
	seeAllUsersButton.innerHTML =
	`
		<div class="col-md-6 offset-md-3 my-2">
			<a href="./allUsers.html" class="btn btn-dark btn-block rounded-0">
				SEE ALL USERS
			</a>
		</div>
	`
}

fetch("https://paintura-service.onrender.com/api/products/all", 
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then( result => result.json())
.then( result => {

		// let courseData;

		if(result.length < 1){

			productData = `No product Available`

		} else {

			productData = result.map( (product) => {

				if(adminUser === false || !adminUser){
					
					if(product.isActive == false ){
						cardFooter =
						`
							<a href="#" class="text-dark selectButton">
								Not Available
							</a>
						`
					} else {
						cardFooter =
						`
							<a href="./singleProduct.html?productId=${product._id}" class="btn btn-dark btn-block rounded-0 border-top-0 border-left-0 border-right-0 selectButton">
								Select product
							</a>
						`
					}

				} else {
					
					if(product.isActive == true){
						cardFooter = 
						`
							<a href="./editProduct.html?productId=${product._id}" class="btn btn-dark btn-block rounded-0 editButton">
								Edit
							</a>
						
							<a href="./archiveProduct.html?productId=${product._id}" class="btn btn-dark btn-block rounded-0 archiveButton">
								Archive
							</a>

							<a href="./deleteProduct.html?productId=${product._id}" class="btn btn-dark btn-block rounded-0 deleteButton">
								Delete
							</a>
						`
					} else {
						cardFooter =
						`
							<a href="./unarchiveProduct.html?productId=${product._id}" class="btn btn-dark btn-block rounded-0 unarchiveButton">
								Unarchive Product
							</a>
							<a href="./deleteProduct.html?productId=${product._id}" class="btn btn-dark btn-block rounded-0 deleteButton">
								Delete Product
							</a>
						`
					}
				}

				return (
					`
					<div class="col-12 col-md-4 my-3">
						<div class="card" >
						  <div class="card-body">
						    <h5 class="card-title text-center font-weight-bold">${product.name}</h5>
						    <p class="card-text text-center">PHP ${product.price}</p>
						    <p class="card-text text-center">${product.desc}</p>
						  </div>
						  <div class="card-footer">
						  	${cardFooter}
						  </div>
						</div>
					</div>

					`
				)
			}).join('');
		}

		let container = document.querySelector('#productContainer');
		container.innerHTML = productData
})
/* ./../images/paint1.jfif*/
/*<img class="card-img-top" src="./../images/paint1.jfif">*/