let registerForm = document.querySelector('#registerForm');

//add event listener
registerForm.addEventListener("submit", (event) => {

	event.preventDefault();


//target the elements of the form and get their values
let name = document.getElementById('name').value;
let email = document.getElementById('email').value;
let password = document.getElementById('password').value;
let password2 = document.getElementById('password2').value;

	if(password === password2){


		// fetch(<url>, {options}).then()
		fetch("https://paintura-service.onrender.com/api/users/checkEmail", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})

			}
		)
		.then(result => result.json())
		.then(result => {
			//result === false
				//means this can be saved in the database

			if(result === false){

				fetch("https://paintura-service.onrender.com/api/users/register",
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							name: name,
							email: email,
							password: password
						})
					}
				)
				.then( result => result.json())
				.then( result => {

					if(result === true){
						alert('Registered successfully!')

						window.location.replace('./login.html');
					} else {
						alert('Something went wrong!')
					}
				})
				
			} else {

				alert("Email already exists");
			}

		})
	}
});
