let params = new URLSearchParams(window.location.search);
let userId = params.get("userId");

fetch(`https://paintura-service.onrender.com/api/users/${userId}/setAsAdmin`, {
  method: "PUT",
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((result) => result.json())
  .then((result) => {
    if (result) {
      alert(`User now an admin!`);
      window.location.replace(`./allUsers.html`);
    } else {
      alert(`Something went wrong.`);
    }
  });
