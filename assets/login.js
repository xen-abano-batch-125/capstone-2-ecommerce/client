let loginUserForm = document.querySelector("#loginUserForm");

loginUserForm.addEventListener("submit", (event) => {
  event.preventDefault();

  let email = document.querySelector("#email").value;
  let password = document.querySelector("#password").value;

  //this is optional coz theres already required in the html
  if (email === "" || password === "") {
    alert("Please input required fields.");
  } else {
    fetch("https://paintura-service.onrender.com/api/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((result) => result.json())
      .then((result) => {
        //save to local storage
        localStorage.setItem("token", result.access);

        let token = result.access;
        if (token) {
          fetch("https://paintura-service.onrender.com/api/users/details", {
            //this is optional coz GET a default
            method: "GET",
            headers: {
              Authorization: `Bearer ${result.access}`,
            },
          })
            .then((result) => result.json())
            .then((result) => {
              localStorage.setItem("id", result._id);
              localStorage.setItem("isAdmin", result.isAdmin);
              window.location.replace("./products.html");
            });
        }
      });
  }
});
